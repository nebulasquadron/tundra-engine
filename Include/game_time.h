#pragma once

#include <chrono>

class GameTime
{
public:
	float deltaTime;
	float totalTime;

	GameTime();

	void Update();
private:
	std::chrono::time_point<std::chrono::steady_clock> lastTime;
};

