#pragma once

#include <memory>

#include <glm/glm.hpp>
#include <GL/glew.h>

#include "text_loader.h"
#include "game_math.h"
#include "color.h"

// TODO: drawCount should be glsizei, and other variables should also use propper opengl types
class Mesh
{
public:
	static unsigned __int64 activeCirculationCount;
private:
	struct MeshBox
	{
		GLuint vertexArrayObject;
		GLuint* vertexBufferObjects;
		unsigned int bufferCount;
		unsigned int drawCount;
		unsigned __int64 circulationID;

		MeshBox(GLuint vertexArrayObject, GLuint* vertexBufferObjects, unsigned int bufferCount, unsigned int drawCount);
		~MeshBox();
	};

	std::shared_ptr<MeshBox> meshBox;
public:
	struct Descriptor
	{
		struct Array
		{
			GLsizeiptr arrDataSize;
			const void* arr;
			GLint typeSize;
			GLenum type;
			bool isStatic;

			Array(GLsizeiptr arrDataSize, const void* arr, GLint typeSize, GLenum type, bool isStatic);
		};

		unsigned short numVertices;
		std::vector<Array> arrs;

		unsigned int numIndices;
		const unsigned short* indices;
		bool areIndicesStatic;

		FVector3 AABBMin, AABBMax;
		float boundingRadius;

		Descriptor(unsigned short numVertices);
		Descriptor(const TextData& data);

		Descriptor& AddArray(const FVector2* arr, bool isStatic = true);
		Descriptor& AddArray(const FVector3* arr, bool isStatic = true);
		Descriptor& AddArray(const FColor* arr, bool isStatic = true);
		Descriptor& AddArray(const float* arr, unsigned int vectorSize, bool isStatic = true);
		Descriptor& AddIndices(const unsigned short* indices, unsigned int numIndices, bool isStatic = true);
		
		Descriptor& AddBounds(const FVector3 AABBMin, const FVector3& AABBMax, float boundingRadius);
		Descriptor& CalculateBoundsFromLastArray();
	};

	// TODO: Put in box
	FVector3 AABBMin, AABBMax;
	float boundingRadius;

	Mesh() :
		AABBMin(-1.0F),
		AABBMax(1.0F),
		boundingRadius(1.0F)
	{ }

	~Mesh() noexcept = default;
	Mesh(const Descriptor& descriptor);
	Mesh(const Mesh&) noexcept = default;
	Mesh(Mesh&&) noexcept = default;
	Mesh& operator=(const Mesh&) noexcept = default;
	Mesh& operator=(Mesh&&) noexcept = default;

	bool IsLoaded() const
	{
		return meshBox != nullptr;
	}

	void Substitute(unsigned int index, const FVector2* arr, unsigned short offset, unsigned short count);
	void Substitute(unsigned int index, const FVector3* arr, unsigned short offset, unsigned short count);
	void Substitute(unsigned int index, const FColor* arr, unsigned short offset, unsigned short count);
	void SubstituteIndices(const unsigned short* indices, unsigned int offset, unsigned int count);

	inline void SetDrawCount(unsigned int drawCount)
	{
		meshBox->drawCount = drawCount;
	}

	inline unsigned int GetDrawCount() const
	{
		return meshBox->drawCount;
	}

	inline void Bind() const
	{
 		glBindVertexArray(meshBox->vertexArrayObject);
	}

	// TODO: mode = GL_TRIANGLES?
	inline void DrawElements() const
	{
		glDrawElements(GL_TRIANGLES, meshBox->drawCount, GL_UNSIGNED_SHORT, 0);
	}

	inline void DrawElements(GLenum mode) const
	{
		glDrawElements(mode, meshBox->drawCount, GL_UNSIGNED_SHORT, 0);
	}

	inline void DrawArrays() const
	{
		glDrawArrays(GL_TRIANGLES, 0, meshBox->drawCount);
	}

	inline void DrawArrays(GLenum mode) const
	{
		glDrawArrays(mode, 0, meshBox->drawCount);
	}

	inline unsigned __int64 GetCirculationID()
	{
		return meshBox->circulationID;
	}
};