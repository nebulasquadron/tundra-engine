#pragma once

#include "general_common.h"
#include "rendering.h"

class PBRMaterial3 : public Material3
{
private:
	Texture2D albedo;
	Texture2D normals;
	Texture2D materialData;
public:
	PBRMaterial3(const Texture2D& albedo, const Texture2D& normals, const Texture2D& materialData) :
		Material3("BasePBR"),
		albedo(albedo),
		normals(normals),
		materialData(materialData)
	{ }

	inline void LoadMaterial() const
	{
		albedo.Bind(3);
		normals.Bind(4);
		materialData.Bind(5);
	}

	no_transfer_functions(PBRMaterial3);
};

class ReflectionProbeComponent3;

namespace Internal
{
	namespace Storage
	{
		struct PBR
		{
			static std::list<ReflectionProbeComponent3*> activeProbes;
		};
	}
}

class ReflectionProbeComponent3 : public Component3
{
public:
	// TODO: Can environment and prefiltered environment can both be the same cubemap?
	Cubemap environmentMap;
	Cubemap irradianceMap;
	Cubemap prefilteredEnvironmentMap;
public:
	ReflectionProbeComponent3();
	ReflectionProbeComponent3(const ReflectionProbeComponent3& other);

	void OnEnable() override;

	void OnDisable() override;

	void Refresh();

	can_copy(ReflectionProbeComponent3)
};

// TODO: Out of header!
class PBROpaqueProcess3 : public RenderingOpaqueProcess3
{
private:
	// TODO: should be reversed, meshes are probably easier to bind than materials (textures)
	typedef std::unordered_map<const Mesh*, std::unordered_map<const PBRMaterial3*, std::list<Transform3*>>> map_type;
	map_type objects;

	const Shader shader;
public:
	PBROpaqueProcess3() :
		shader(GenerateShaderCode::ObjectVertex(), GenerateShaderCode::ObjectFragment(false))
	{ }

	void Run(const RMatrix4x4& projectionView, const Camera3* camera) override;

	void AddObject(const RenderComponent3& object) override
	{
		objects[&object.GetMesh()][reinterpret_cast<const PBRMaterial3*>(object.GetMaterial())].push_back(&object.gameObject->transform);
	}

	void RemoveObject(const RenderComponent3& object) override
	{
		/*int instances = 0;
		for (auto* p : objects[object.GetMesh()][object.GetMaterial()])
		{
		if (p == &object.gameObject->transform)
		instances++;
		}
		if (instances != 1)
		std::cerr << "INST ERR " << instances << std::endl;*/
		
		auto meshPair = objects.find(&object.GetMesh());
		auto materialPair = meshPair->second.find(reinterpret_cast<const PBRMaterial3*>(object.GetMaterial()));
		materialPair->second.remove(&object.gameObject->transform);
		if (materialPair->second.empty())
		{
			meshPair->second.erase(materialPair);
			if (meshPair->second.empty())
				objects.erase(meshPair);
		}
	}

	/*void* Suspend() override;
	void Resume(void* data) override;
	void Swap(void* data) override;
	void Deallocate(void* data) override;*/
};

namespace Internal
{
	namespace PBR
	{
		void Init();
	}
}