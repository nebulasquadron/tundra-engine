#pragma once
class RReal
{
public:
	RReal(__int64 seed);
	RReal();
	~RReal();

	void operator=(__int64 seed);
	double operator()(__int32 x, __int32 y);

	// TODO: Make const
	long double GetLongDouble(__int32 x, __int32 y);
	double GetDouble(__int32 x, __int32 y);
	float GetFloat(__int32 x, __int32 y);
	__int8 GetByte(__int32 x, __int32 y);
	__int16 GetShort(__int32 x, __int32 y);
	__int32 GetInt(__int32 x, __int32 y);
	__int64 GetLong(__int32 x, __int32 y);
private:
	inline __int64 Init(__int64 seed) const;

	__int64 seed;

	static const __int64 MULTIPLIER;
	static const __int64 ADDEND;
	static const __int64 MASK;
	static const double DOUBLE_UNIT;
	static const float FLOAT_UNIT;
};