#pragma once
#include "rreal.h"

class ValueGenerator
{
public:
	ValueGenerator();
	~ValueGenerator();

	void operator=(__int64 seed);
	double operator()(double x, double y);
private:
	double SmoothNoise(__int32 x, __int32 y);
	double Interpolate(double a, double b, double blend);

	RReal generator;
};