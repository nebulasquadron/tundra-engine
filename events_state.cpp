#include "Include/events_state.h"

bool EventsState::isProgramActive = true;
const unsigned __int8* EventsState::internalKeyStates;
unsigned __int8* EventsState::keyStates;
std::vector<SDL_Scancode> EventsState::pressedKeys;
std::vector<SDL_Scancode> EventsState::releasedKeys;
bool EventsState::mouseButtonStates[3] = { false, false, false };
bool EventsState::pressedMouseButtons[3] = { false, false, false };
bool EventsState::releasedMouseButtons[3] = { false, false, false };
int EventsState::mouseX = 0;
int EventsState::mouseY = 0;
int EventsState::mouseDX = 0;
int EventsState::mouseDY = 0;
int EventsState::mouseDM = 0;
int EventsState::windowWidth = 0;
int EventsState::windowHeight = 0;
unsigned int EventsState::cursorFreeLockCount;

void EventsState::Init(int windowWidth, int windowHeight)
{
	EventsState::windowWidth = windowWidth;
	EventsState::windowHeight = windowHeight;
	internalKeyStates = SDL_GetKeyboardState(nullptr);

	keyStates = new unsigned __int8[SDL_NUM_SCANCODES];
	for (unsigned int i = 0; i < SDL_NUM_SCANCODES; ++i)
		keyStates[i] = 0;
}

void EventsState::Clear()
{
	delete[] keyStates;
}

void EventsState::Update()
{
	mouseDX = 0;
	mouseDY = 0;
	mouseDM = 0;
	pressedMouseButtons[0] = false;
	pressedMouseButtons[1] = false;
	pressedMouseButtons[2] = false;
	releasedMouseButtons[0] = false;
	releasedMouseButtons[1] = false;
	releasedMouseButtons[2] = false;
	pressedKeys.clear();
	releasedKeys.clear();
	SDL_Event e;
	// WARNING: button < 3 may not be necessary
	while (SDL_PollEvent(&e))
	{
		switch (e.type)
		{
		case SDL_MOUSEMOTION:
			mouseDX += e.motion.xrel;
			mouseDY += e.motion.yrel;
			break;
		case SDL_MOUSEWHEEL:
			mouseDM += e.wheel.y;
			break;
		case SDL_MOUSEBUTTONDOWN:
		{
			unsigned __int8 button = e.button.button - 1;
			if (button < 3)
			{
				mouseButtonStates[button] = true;
				pressedMouseButtons[button] = true;
			}
			break;
		}
		case SDL_MOUSEBUTTONUP:
		{
			unsigned __int8 button = e.button.button - 1;
			if (button < 3)
			{
				mouseButtonStates[button] = false;
				releasedMouseButtons[button] = true;
			}
			break;
		}
		case SDL_KEYDOWN:
			if (!keyStates[e.key.keysym.scancode])
				pressedKeys.push_back(e.key.keysym.scancode);
			break;
		case SDL_KEYUP:
			releasedKeys.push_back(e.key.keysym.scancode);
			break;
		case SDL_QUIT:
			isProgramActive = false;
			break;
		default:
			break;
		}
	}
	SDL_GetMouseState(&mouseX, &mouseY);
	// TODO: Whole array does not need to be copied
	memcpy(keyStates, internalKeyStates, SDL_NUM_SCANCODES);
}