#include "Include/game_object.h"

/*std::list<TGameObject<Transform2>*> Internal::Storage::GameObjects<Transform2>::objects;
std::list<TGameObject<Transform2>*> Internal::Storage::GameObjects<Transform2>::destructionQueue;
std::list<TGameObject<Transform3>*> Internal::Storage::GameObjects<Transform3>::objects;
std::list<TGameObject<Transform3>*> Internal::Storage::GameObjects<Transform3>::;*/

MemoryPool<TGameObject<Transform2>> Internal::Storage::GameObjects<Transform2>::objects;
std::vector<TGameObject<Transform2>*> Internal::Storage::GameObjects<Transform2>::destructionQueueA;
std::vector<TGameObject<Transform2>*> Internal::Storage::GameObjects<Transform2>::destructionQueueB;
std::vector<TGameObject<Transform2>*> Internal::Storage::GameObjects<Transform2>::destructionQueueC;

MemoryPool<TGameObject<Transform3>> Internal::Storage::GameObjects<Transform3>::objects;
std::vector<TGameObject<Transform3>*> Internal::Storage::GameObjects<Transform3>::destructionQueueA;
std::vector<TGameObject<Transform3>*> Internal::Storage::GameObjects<Transform3>::destructionQueueB;
std::vector<TGameObject<Transform3>*> Internal::Storage::GameObjects<Transform3>::destructionQueueC;
