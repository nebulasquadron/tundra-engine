#include "Include/mesh.h"

#include "Include/debug.h"

unsigned __int64 Mesh::activeCirculationCount = 0ui64;

Mesh::MeshBox::MeshBox(GLuint vertexArrayObject, GLuint* vertexBufferObjects, unsigned int bufferCount, unsigned int drawCount) :
	vertexArrayObject(vertexArrayObject),
	vertexBufferObjects(vertexBufferObjects),
	bufferCount(bufferCount),
	drawCount(drawCount)
{
	static unsigned __int64 totalCirculationIDCount = 0ui64;
	circulationID = totalCirculationIDCount++;
	++activeCirculationCount;
}

Mesh::MeshBox::~MeshBox()
{
	--activeCirculationCount;
	glDeleteBuffers(bufferCount, vertexBufferObjects);
	glDeleteVertexArrays(1, &vertexArrayObject);
	delete[] vertexBufferObjects;
}

Mesh::Descriptor::Array::Array(GLsizeiptr arrDataSize, const void* arr, GLint typeSize, GLenum type, bool isStatic) :
	arrDataSize(arrDataSize),
	arr(arr),
	typeSize(typeSize),
	type(type),
	isStatic(isStatic)
{ }

Mesh::Descriptor::Descriptor(unsigned short numVertices)
{
	this->numVertices = numVertices;
	indices = nullptr;
	numIndices = 0;

	AABBMin = FVector3(-1.0F, -1.0F, -1.0F);
	AABBMax = FVector3(1.0F, 1.0F, 1.0F);
	boundingRadius = 1.0F;
}

Mesh::Descriptor::Descriptor(const TextData& data)
{
	FloatData positions = data.GetFloats("pos");
	FloatData texCoords = data.GetFloats("tex");
	FloatData normals = data.GetFloats("nor");
	FloatData tangents = data.GetFloats("tan");
	UIntData indices = data.GetUInts("ind");
	FloatData bounds = data.GetFloats("bounds");

	numVertices = positions.count / 3;
	if (positions.count == 0)
		Console::Error("All mesh vertices are missing...");
	AddArray(positions.floats, 3);
	if (texCoords.count != 0)
		AddArray(texCoords.floats, 2);
	if (normals.count != 0)
		AddArray(normals.floats, 3);
	if (tangents.count != 0)
		AddArray(tangents.floats, 3);
	if (indices.count != 0)
	{
		this->indices = indices.ints;
		numIndices = indices.count;
	}
	else
	{
		this->indices = nullptr;
	}
	if (bounds.count == 7)
	{
		AABBMin = FVector3(bounds.floats[0], bounds.floats[1], bounds.floats[2]);
		AABBMax = FVector3(bounds.floats[3], bounds.floats[4], bounds.floats[5]);
		boundingRadius = bounds.floats[6];
	}
	else
	{
		AABBMin = FVector3(-1.0F, -1.0F, -1.0F);
		AABBMax = FVector3(1.0F, 1.0F, 1.0F);
		boundingRadius = 1.0F;
	}
}

Mesh::Descriptor& Mesh::Descriptor::AddArray(const FVector2* arr, bool isStatic)
{
	arrs.push_back(Array(numVertices * sizeof(FVector2), arr, 2, GL_FLOAT, isStatic));
	
	return *this;
}

Mesh::Descriptor& Mesh::Descriptor::AddArray(const FVector3* arr, bool isStatic)
{
	arrs.push_back(Array(numVertices * sizeof(FVector3), arr, 3, GL_FLOAT, isStatic));

	return *this;
}

Mesh::Descriptor& Mesh::Descriptor::AddArray(const FColor* arr, bool isStatic)
{
	arrs.push_back(Array(numVertices * sizeof(FColor), arr, 4, GL_FLOAT, isStatic));

	return *this;
}

Mesh::Descriptor& Mesh::Descriptor::AddArray(const float* arr, unsigned int vectorSize, bool isStatic)
{
	arrs.push_back(Array(numVertices * sizeof(float) * vectorSize, arr, vectorSize, GL_FLOAT, isStatic));

	return *this;
}

Mesh::Descriptor& Mesh::Descriptor::AddIndices(const unsigned short* indices, unsigned int numIndices, bool isStatic)
{
	this->indices = indices;
	this->numIndices = numIndices;
	areIndicesStatic = isStatic;

	return *this;
}

Mesh::Descriptor& Mesh::Descriptor::AddBounds(const FVector3 AABBMin, const FVector3& AABBMax, float boundingRadius)
{
	this->AABBMin = AABBMin;
	this->AABBMax = AABBMax;
	this->boundingRadius = boundingRadius;

	return *this;
}

// TODO: Remove

#include <iostream>


Mesh::Mesh(const Descriptor& descriptor)
{
	AABBMin = descriptor.AABBMin;
	AABBMax = descriptor.AABBMax;
	boundingRadius = descriptor.boundingRadius;

	unsigned int bufferCount = static_cast<unsigned int>(descriptor.arrs.size());
	unsigned int drawCount = descriptor.numVertices;
	if (descriptor.indices != nullptr)
	{
		++bufferCount;
		drawCount = descriptor.numIndices;
	}

	GLuint vertexArrayObject;
	glGenVertexArrays(1, &vertexArrayObject);
	glBindVertexArray(vertexArrayObject);

	GLuint* vertexBufferObjects = new GLuint[bufferCount];
	glGenBuffers(bufferCount, vertexBufferObjects);

	for (unsigned int i = 0; i < descriptor.arrs.size(); ++i)
	{
		glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObjects[i]);
		glBufferData(GL_ARRAY_BUFFER, descriptor.arrs[i].arrDataSize, descriptor.arrs[i].arr, descriptor.arrs[i].isStatic ? GL_STATIC_DRAW : GL_DYNAMIC_DRAW);
		glEnableVertexAttribArray(i);
		glVertexAttribPointer(i, descriptor.arrs[i].typeSize, descriptor.arrs[i].type, GL_FALSE, 0, 0);
	}

	if (descriptor.indices != nullptr)
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vertexBufferObjects[bufferCount - 1]);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, descriptor.numIndices * sizeof(unsigned short), descriptor.indices, descriptor.areIndicesStatic ? GL_STATIC_DRAW : GL_DYNAMIC_DRAW);
	}

	glBindVertexArray(0);

	meshBox = std::shared_ptr<MeshBox>(new MeshBox(vertexArrayObject, vertexBufferObjects, bufferCount, drawCount));
}

void Mesh::Substitute(unsigned int index, const FVector2* arr, unsigned short offset, unsigned short count)
{
	glBindBuffer(GL_ARRAY_BUFFER, meshBox->vertexBufferObjects[index]);
	glBufferSubData(GL_ARRAY_BUFFER, offset, count * sizeof(FVector2), arr);
}

void Mesh::Substitute(unsigned int index, const FVector3* arr, unsigned short offset, unsigned short count)
{
	glBindBuffer(GL_ARRAY_BUFFER, meshBox->vertexBufferObjects[index]);

	//GLint size = 0;
	//glGetBufferParameteriv(GL_ARRAY_BUFFER, GL_BUFFER_SIZE, &size);
	//std::cerr << index << " " << offset << " " << count * sizeof(FVector3) << " " << size << std::endl;
	glBufferSubData(GL_ARRAY_BUFFER, offset, count * sizeof(FVector3), arr);
}

void Mesh::Substitute(unsigned int index, const FColor* arr, unsigned short offset, unsigned short count)
{
	glBindBuffer(GL_ARRAY_BUFFER, meshBox->vertexBufferObjects[index]);
	glBufferSubData(GL_ARRAY_BUFFER, offset, count * sizeof(FColor), arr);
}

void Mesh::SubstituteIndices(const unsigned short* indices, unsigned int offset, unsigned int count)
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, meshBox->vertexBufferObjects[meshBox->bufferCount - 1]);
	glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, offset, count * sizeof(unsigned short), indices);
}