#include "Include/pbr.h"

std::list<ReflectionProbeComponent3*> Internal::Storage::PBR::activeProbes;

ReflectionProbeComponent3::ReflectionProbeComponent3() :
	environmentMap(Cubemap::Descriptor(Internal::Storage::Rendering::ENVIRONMENT_MAP_RESOLUTION, Internal::Storage::Rendering::ENVIRONMENT_MAP_RESOLUTION, GL_RGB16F, true, false)),
	irradianceMap(Cubemap::Descriptor(Internal::Storage::Rendering::IRRADIANCE_MAP_RESOLUTION, Internal::Storage::Rendering::IRRADIANCE_MAP_RESOLUTION, GL_RGB16F, true, false)),
	prefilteredEnvironmentMap(Cubemap::Descriptor(Internal::Storage::Rendering::PREFILTERED_ENVIRONMENT_MAP_RESOLUTION, Internal::Storage::Rendering::PREFILTERED_ENVIRONMENT_MAP_RESOLUTION, GL_RGB16F, true, true))
{ }

ReflectionProbeComponent3::ReflectionProbeComponent3(const ReflectionProbeComponent3& other) :
	environmentMap(other.environmentMap),
	irradianceMap(other.irradianceMap)
{ }

void ReflectionProbeComponent3::OnEnable()
{
	Internal::Storage::PBR::activeProbes.push_back(this);
}

void ReflectionProbeComponent3::OnDisable()
{
	Internal::Storage::PBR::activeProbes.remove(this);
}

void ReflectionProbeComponent3::Refresh()
{
	Scene::ConductProbe(gameObject->transform, environmentMap);
	Scene::ComputeIrradiance(environmentMap, irradianceMap);
	Scene::ComputePrefilteredEnvironmentMap(environmentMap, prefilteredEnvironmentMap);
}

void PBROpaqueProcess3::Run(const RMatrix4x4& projectionView, const Camera3* camera)
{
	shader.Bind();
	if (Internal::Storage::RenderingObj::directionalLight != nullptr)
	{
		Shader::LoadVector3(4, Internal::Storage::RenderingObj::directionalLight->gameObject->transform.GetWorldForward());
		Shader::LoadVector3(5, &Internal::Storage::RenderingObj::directionalLight->color.r);
	}
	else
	{
		Shader::LoadVector3(4, FVector3(0.0F, -1.0F, 0.0F));
		Shader::LoadVector3(5, FVector3(0.0F, 0.0F, 0.0F));
	}

	Internal::Storage::Rendering::brdfLUT.Bind(0);
	if (!Internal::Storage::PBR::activeProbes.empty())
	{
		Internal::Storage::PBR::activeProbes.front()->prefilteredEnvironmentMap.Bind(1);
		Internal::Storage::PBR::activeProbes.front()->irradianceMap.Bind(2);
	}
	else
	{
		Internal::Storage::Rendering::defaultCubemap.Bind(1);
		Internal::Storage::Rendering::defaultCubemap.Bind(2);
	}
	RVector3 camPos = camera->gameObject->transform.GetWorldPosition();
	for (auto& meshPair : objects)
	{
		meshPair.first->Bind();
		for (auto& materialPair : meshPair.second)
		{
			materialPair.first->LoadMaterial();
			for (Transform3* transform : materialPair.second)
			{
				//std::cerr << glm::to_string(projectionView * transform->GetMatrix()) << std::endl;

				Shader::LoadMatrix4x4(0, static_cast<FMatrix4x4>(projectionView * transform->GetMatrix()));
				Shader::LoadMatrix3x3(1, transform->GetWorldRotationMatrix3x3());
				Shader::LoadVector3(2, transform->GetWorldScale());
				Shader::LoadVector3(3, static_cast<FVector3>(camPos - transform->GetWorldPosition()));
				meshPair.first->DrawElements();
			}
		}
	}
}

/*void* PBROpaqueProcess3::Suspend()
{
	return new map_type(std::move(objects));
}

void PBROpaqueProcess3::Resume(void* data)
{
	objects = std::move(*reinterpret_cast<map_type*>(data));
}

void PBROpaqueProcess3::Swap(void* data)
{
	std::swap(objects, *reinterpret_cast<map_type*>(data));
}

void PBROpaqueProcess3::Deallocate(void* data)
{
	delete reinterpret_cast<map_type*>(data);
}*/

namespace Internal
{
	namespace PBR
	{
		void Init()
		{
			std::vector<ObjectRenderingProcess3*> basePBRProcess;
			basePBRProcess.push_back(new PBROpaqueProcess3());
			Rendering::RegisterProcess("BasePBR", basePBRProcess);
		}
	}
}