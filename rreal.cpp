#include "Include/rreal.h"

#include <iostream>
#include <string>

RReal::RReal(__int64 seed)
{
	//std::cout << seed << " " << MULTIPLIER << " " << (seed ^ MULTIPLIER) << std::endl;
	this->seed = Init(seed);
	//std::cout << this->seed << " " << MULTIPLIER << " " << MASK << " " << (seed ^ MULTIPLIER) << std::endl;
}

RReal::RReal()
{
	this->seed = 0i64;
}

RReal::~RReal() {}

void RReal::operator=(__int64 seed)
{
	this->seed = seed;
}

double RReal::operator()(__int32 x, __int32 y)
{
	return GetDouble(x, y);
}

long double RReal::GetLongDouble(__int32 x, __int32 y)
{
	return ((long double) GetDouble(x, y)) * ((long double) GetDouble(x + 1, y));
}

double RReal::GetDouble(__int32 x, __int32 y)
{
	unsigned __int64 seed0 = ((unsigned __int64)(Init(this->seed + (x * 49632i32 + y * 325176i32)) * MULTIPLIER + ADDEND) & MASK);
	unsigned __int64 seed1 = ((unsigned __int64)(seed0 * MULTIPLIER + ADDEND) & MASK);
	unsigned __int32 r0 = (unsigned __int32)(seed0 >> (48i64 - 26i64));
	unsigned __int32 r1 = (unsigned __int32)(seed1 >> (48i64 - 27i64));
	return (((unsigned __int64)(r0) << 27i64) + r1) * DOUBLE_UNIT;
}

float RReal::GetFloat(__int32 x, __int32 y)
{
	unsigned __int64 seed0 = ((unsigned __int64)(Init(this->seed + (x * 49632i32 + y * 325176i32)) * MULTIPLIER + ADDEND) & MASK);
	unsigned __int32 r0 = (unsigned __int32)(seed0 >> (48i64 - 24i64));
	return r0 * FLOAT_UNIT;
}

__int8 RReal::GetByte(__int32 x, __int32 y)
{
	unsigned __int64 seed0 = ((unsigned __int64)(Init(this->seed + (x * 49632i32 + y * 325176i32)) * MULTIPLIER + ADDEND) & MASK);
	return (unsigned __int8)(seed0 >> (48i64 - 8i64));
}

__int16 RReal::GetShort(__int32 x, __int32 y)
{
	unsigned __int64 seed0 = ((unsigned __int64)(Init(this->seed + (x * 49632i32 + y * 325176i32)) * MULTIPLIER + ADDEND) & MASK);
	return (unsigned __int16)(seed0 >> (48i64 - 16i64));
}

__int32 RReal::GetInt(__int32 x, __int32 y)
{
	unsigned __int64 seed0 = ((unsigned __int64)(Init(this->seed + (x * 49632i32 + y * 325176i32)) * MULTIPLIER + ADDEND) & MASK);
	return (unsigned __int32)(seed0 >> (48i64 - 32i64));
}

__int64 RReal::GetLong(__int32 x, __int32 y)
{
	unsigned __int64 seed0 = ((unsigned __int64)(Init(this->seed + (x * 49632i32 + y * 325176i32)) * MULTIPLIER + ADDEND) & MASK);
	unsigned __int64 seed1 = ((unsigned __int64)(seed0 * MULTIPLIER + ADDEND) & MASK);
	unsigned __int32 r0 = (unsigned __int32)(seed0 >> (48i64 - 32i64));
	unsigned __int32 r1 = (unsigned __int32)(seed1 >> (48i64 - 32i64));
	return (((unsigned __int64)r0) << 32i64) + (unsigned __int64)r1;
}

inline __int64 RReal::Init(__int64 seed) const
{
	return (seed ^ MULTIPLIER) & MASK;
}

const __int64 RReal::MULTIPLIER = 0x5DEECE66Di64;
const __int64 RReal::ADDEND = 0xBi64;
const __int64 RReal::MASK = (1i64 << 48i64) - 1i64;
const double RReal::DOUBLE_UNIT = 1.0 / (1i64 << 53i64);//1.0e-53;
const float RReal::FLOAT_UNIT = 1.0f / (1i32 << 24i32);